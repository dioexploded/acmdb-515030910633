package simpledb;

import java.util.ArrayList;


/**
 * A class to represent a fixed-width histogram over a single integer-based field.
 */
public class IntHistogram
{

	int[] hist;
	int[] split;
	int n;
	int num;

	/**
	 * Create a new IntHistogram.
	 * <p>
	 * This IntHistogram should maintain a histogram of integer values that it receives.
	 * It should split the histogram into "buckets" buckets.
	 * <p>
	 * The values that are being histogrammed will be provided one-at-a-time through the "addValue()" function.
	 * <p>
	 * Your implementation should use space and have execution time that are both
	 * constant with respect to the number of values being histogrammed.  For example, you shouldn't
	 * simply store every value that you see in a sorted list.
	 *
	 * @param buckets The number of buckets to split the input value into.
	 * @param min     The minimum integer value that will ever be passed to this class for histogramming
	 * @param max     The maximum integer value that will ever be passed to this class for histogramming
	 */
	public IntHistogram(int buckets, int min, int max)
	{
		// some code goes here
		num = 0;
		n = buckets;
		hist = new int[buckets];
		for (int i = 0; i < buckets; i++)
			hist[i] = 0;
		split = new int[buckets + 1];
		int left = min, right = max + 1;
		for (int i = 0; i <= buckets; i++)
			split[i] = (int)(((long)(buckets - i) * left + (long)i * right) / buckets);

	}

	/**
	 * Add a value to the set of values that you are keeping a histogram of.
	 *
	 * @param v Value to add to the histogram
	 */
	public void addValue(int v)
	{
		// some code goes here
		num++;
		int l = 0, r = n - 1;
		int mid = (l + r) / 2;
		while (r - l > 0)
		{
			if (v >= split[mid] && v < split[mid + 1])
				break;
			if (v < split[mid])
				r = mid - 1;
			else
				l = mid + 1;
			mid = (l + r) / 2;
		}
		assert (v >= split[mid] && v < split[mid + 1]);
		hist[mid]++;
		//assert (false);
	}

	/**
	 * Estimate the selectivity of a particular predicate and operand on this table.
	 * <p>
	 * For example, if "op" is "GREATER_THAN" and "v" is 5,
	 * return your estimate of the fraction of elements that are greater than 5.
	 *
	 * @param op Operator
	 * @param v  Value
	 * @return Predicted selectivity of this particular operator and value
	 */
	public double estimateSelectivity(Predicate.Op op, int v)
	{

		// some code goes here
		if (op == Predicate.Op.EQUALS || op == Predicate.Op.NOT_EQUALS)
		{
			double e = 0.0;
			for (int i = 0; i < n; i++)
				if (v >= split[i] && v < split[i + 1])
				{
					e = 1.0 * hist[i] / (split[i + 1] - split[i]) / num;
					break;
				}
			if (op == Predicate.Op.EQUALS)
				return e;
			else
				return 1.0 - e;
		}
		else
		{
			double l = 0.0, r = 0.0;
			for (int i = 0; i < n; i++)
			{
				if (v > split[i])
					l += 1.0 * ((v <= split[i + 1] ? v : split[i + 1]) - split[i]) / (split[i + 1] - split[i]) * hist[i] / num;
				if (v < split[i + 1] - 1)
					r += 1.0 * (split[i + 1] - (v + 1 >= split[i] ? v + 1 : split[i])) / (split[i + 1] - split[i]) * hist[i] / num;
			}
			switch (op)
			{
			case LESS_THAN:
				return l;
			case GREATER_THAN:
				return r;
			case LESS_THAN_OR_EQ:
				return (1.0 - r);
			case GREATER_THAN_OR_EQ:
				return (1.0 - l);
			}
		}
		assert (false);
		return -1.0;
	}

	/**
	 * @return the average selectivity of this histogram.
	 * <p>
	 * This is not an indispensable method to implement the basic
	 * join optimization. It may be needed if you want to
	 * implement a more efficient optimization
	 */
	public double avgSelectivity()
	{
		// some code goes here
		return 1.0;
	}

	/**
	 * @return A string describing this histogram, for debugging purposes
	 */
	public String toString()
	{
		// some code goes here
		return null;
	}
}
