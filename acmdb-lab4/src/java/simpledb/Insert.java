package simpledb;

/**
 * Inserts tuples read from the child operator into the tableId specified in the
 * constructor
 */
public class Insert extends Operator
{

	private static final long serialVersionUID = 1L;
	TransactionId t;
	DbIterator child;
	int tableId;
	boolean inserted;
	/**
	 * Constructor.
	 *
	 * @param pt       The transaction running the insert.
	 * @param pchild   The child operator from which to read tuples to be inserted.
	 * @param ptableId The table in which to insert tuples.
	 * @throws DbException if TupleDesc of child differs from table into which we are to
	 *                     insert.
	 */
	public Insert(TransactionId pt, DbIterator pchild, int ptableId) throws DbException
	{
		// some code goes here
		t=pt;
		child=pchild;
		tableId=ptableId;
		inserted=false;
	}

	public TupleDesc getTupleDesc()
	{
		// some code goes here
		return new TupleDesc(new Type[]{Type.INT_TYPE});
	}

	public void open() throws DbException, TransactionAbortedException
	{
		// some code goes here
		super.open();
		child.open();
		inserted=false;
	}

	public void close()
	{
		// some code goes here
		child.close();
		super.close();
	}

	public void rewind() throws DbException, TransactionAbortedException
	{
		// some code goes here
		child.rewind();
		inserted=false;
	}

	/**
	 * Inserts tuples read from child into the tableId specified by the
	 * constructor. It returns a one field tuple containing the number of
	 * inserted records. Inserts should be passed through BufferPool. An
	 * instances of BufferPool is available via Database.getBufferPool(). Note
	 * that insert DOES NOT need check to see if a particular tuple is a
	 * duplicate before inserting it.
	 *
	 * @return A 1-field tuple containing the number of inserted records, or
	 * null if called more than once.
	 * @see Database#getBufferPool
	 * @see BufferPool#insertTuple
	 */
	protected Tuple fetchNext() throws TransactionAbortedException, DbException
	{
		// some code goes here
		if(inserted) return null;
		BufferPool pool=Database.getBufferPool();
		int count=0;
		while(child.hasNext())
		{
			try
			{
				pool.insertTuple(t,tableId,child.next());
				count++;
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

		}
		inserted=true;
		Tuple ret=new Tuple(new TupleDesc(new Type[]{Type.INT_TYPE}));
		ret.setField(0,new IntField(count));
		return ret;
	}

	@Override
	public DbIterator[] getChildren()
	{
		// some code goes here
		return new DbIterator[]{child};
	}

	@Override
	public void setChildren(DbIterator[] children)
	{
		// some code goes here
		assert children.length==1;
		child=children[0];
	}
}
