package simpledb;

import java.util.*;

/**
 * SeqScan is an implementation of a sequential scan access method that reads
 * each tuple of a table in no particular order (e.g., as they are laid out on
 * disk).
 */
public class SeqScan implements DbIterator
{

	private static final long serialVersionUID = 1L;

	private TransactionId tid;
	private int tableid;
	private String tableAlias;
	private DbFileIterator dbfIter;

	/**
	 * Creates a sequential scan over the specified table as a part of the
	 * specified transaction.
	 *
	 * @param ptid        The transaction this scan is running as a part of.
	 * @param ptableid    the table to scan.
	 * @param ptableAlias the alias of this table (needed by the parser); the returned
	 *                    tupleDesc should have fields with name tableAlias.fieldName
	 *                    (note: this class is not responsible for handling a case where
	 *                    tableAlias or fieldName are null. It shouldn't crash if they
	 *                    are, but the resulting name can be null.fieldName,
	 *                    tableAlias.null, or null.null).
	 */
	public SeqScan(TransactionId ptid, int ptableid, String ptableAlias)
	{
		// some code goes here
		tid = ptid;
		tableid = ptableid;
		tableAlias = ptableAlias;
		dbfIter=Database.getCatalog().getDatabaseFile(tableid).iterator(tid);
	}

	/**
	 * @return return the table name of the table the operator scans. This should
	 * be the actual name of the table in the catalog of the database
	 */
	public String getTableName()
	{
		return Database.getCatalog().getTableName(tableid);
	}

	/**
	 * @return Return the alias of the table this operator scans.
	 */
	public String getAlias()
	{
		// some code goes here
		return tableAlias;
	}

	/**
	 * Reset the tableid, and tableAlias of this operator.
	 *
	 * @param ptableid    the table to scan.
	 * @param ptableAlias the alias of this table (needed by the parser); the returned
	 *                   tupleDesc should have fields with name tableAlias.fieldName
	 *                   (note: this class is not responsible for handling a case where
	 *                   tableAlias or fieldName are null. It shouldn't crash if they
	 *                   are, but the resulting name can be null.fieldName,
	 *                   tableAlias.null, or null.null).
	 */
	public void reset(int ptableid, String ptableAlias)
	{
		// some code goes here
		tableid=ptableid;
		tableAlias=ptableAlias;
	}

	public SeqScan(TransactionId tid, int tableid)
	{
		this(tid, tableid, Database.getCatalog().getTableName(tableid));
	}

	public void open() throws DbException, TransactionAbortedException
	{
		// some code goes here
		dbfIter.open();
	}

	/**
	 * Returns the TupleDesc with field names from the underlying HeapFile,
	 * prefixed with the tableAlias string from the constructor. This prefix
	 * becomes useful when joining tables containing a field(s) with the same
	 * name.  The alias and name should be separated with a "." character
	 * (e.g., "alias.fieldName").
	 *
	 * @return the TupleDesc with field names from the underlying HeapFile,
	 * prefixed with the tableAlias string from the constructor.
	 */
	public TupleDesc getTupleDesc()
	{
		// some code goes here
		return Database.getCatalog().getTupleDesc(tableid);
	}

	public boolean hasNext() throws TransactionAbortedException, DbException
	{
		// some code goes here
		return dbfIter.hasNext();
	}

	public Tuple next() throws NoSuchElementException, TransactionAbortedException, DbException
	{
		// some code goes here
		return dbfIter.next();
	}

	public void close()
	{
		// some code goes here
		dbfIter.close();
	}

	public void rewind() throws DbException, NoSuchElementException, TransactionAbortedException
	{
		// some code goes here
		dbfIter.rewind();
	}
}
