package simpledb;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Knows how to compute some aggregate over a set of IntFields.
 */
public class IntegerAggregator implements Aggregator
{

	private static final long serialVersionUID = 1L;

	private class Val
	{

		private int count;
		private int sum;
		private int max;
		private int min;

		public Val()
		{
			count = 0;
			sum = 0;
			max = Integer.MIN_VALUE;
			min = Integer.MAX_VALUE;
		}

		public void add(int x)
		{
			count++;
			sum += x;
			max = max > x ? max : x;
			min = min < x ? min : x;
		}
		public int get(Op what)
		{
			if (what.equals(Op.MIN))
				return min;
			if (what.equals(Op.AVG))
				return sum / count;
			if (what.equals(Op.COUNT))
				return count;
			if (what.equals(Op.MAX))
				return max;
			if (what.equals(Op.SUM))
				return sum;
			return 0;
		}
	}

	private int gbfield;
	private Type gbfieldtype;
	private int afield;
	private Op what;
	private Val SingleVal;
	private HashMap<Integer, Val> IValMap;
	private HashMap<String,Val> SValMap;
	private TupleDesc td;

	/**
	 * Aggregate constructor
	 *
	 * @param pgbfield     the 0-based index of the group-by field in the tuple, or
	 *                     NO_GROUPING if there is no grouping
	 * @param pgbfieldtype the type of the group by field (e.g., Type.INT_TYPE), or null
	 *                     if there is no grouping
	 * @param pafield      the 0-based index of the aggregate field in the tuple
	 * @param pwhat        the aggregation operator
	 */

	public IntegerAggregator(int pgbfield, Type pgbfieldtype, int pafield, Op pwhat)
	{
		// some code goes here
		//assert  pgbfieldtype==Type.INT_TYPE;
		gbfield = pgbfield;
		gbfieldtype = pgbfieldtype;
		afield = pafield;
		what = pwhat;
		if(pgbfield==NO_GROUPING)
		{
			SingleVal=new Val();
			td=new TupleDesc(new Type[]{Type.INT_TYPE});
		}
		else if(pgbfieldtype==Type.INT_TYPE)
		{
			IValMap=new HashMap<>();
			td=new TupleDesc(new Type[]{Type.INT_TYPE,Type.INT_TYPE});
		}
		else
		{
			SValMap=new HashMap<>();
			td=new TupleDesc(new Type[]{Type.STRING_TYPE,Type.INT_TYPE});
		}
	}

	public TupleDesc getTd()
	{
		return td;
	}
	/**
	 * Merge a new tuple into the aggregate, grouping as indicated in the
	 * constructor
	 *
	 * @param tup the Tuple containing an aggregate field and a group-by field
	 */
	public void mergeTupleIntoGroup(Tuple tup)
	{
		// some code goes here
		if(gbfield==NO_GROUPING)
			SingleVal.add(((IntField) tup.getField(afield)).getValue());
		else if(gbfieldtype==Type.INT_TYPE)
		{
			int key = ((IntField) tup.getField(gbfield)).getValue();
			if (!IValMap.containsKey(key))
				IValMap.put(key, new Val());
			IValMap.get(key).add(((IntField) tup.getField(afield)).getValue());
		}
		else
		{
			String key = ((StringField) tup.getField(gbfield)).getValue();
			if (!SValMap.containsKey(key))
				SValMap.put(key, new Val());
			SValMap.get(key).add(((IntField) tup.getField(afield)).getValue());
		}

	}

	/**
	 * Create a DbIterator over group aggregate results.
	 *
	 * @return a DbIterator whose tuples are the pair (groupVal, aggregateVal)
	 * if using group, or a single (aggregateVal) if no grouping. The
	 * aggregateVal is determined by the type of aggregate specified in
	 * the constructor.
	 */
	public class IntAggIter implements DbIterator
	{
		ArrayList<Tuple> a;
		Iterator<Tuple> iter;
		TupleDesc desc;
		public IntAggIter(ArrayList<Tuple> pa,TupleDesc pd)
		{
			a=pa;
			desc=pd;
			iter=null;
		}
		public void open() throws DbException, TransactionAbortedException
		{
			iter=a.iterator();
		}


		public boolean hasNext() throws DbException, TransactionAbortedException
		{
			if(iter==null) throw new IllegalStateException();
			return iter.hasNext();
		}

		public Tuple next() throws DbException, TransactionAbortedException, NoSuchElementException
		{
			if(iter==null) throw new IllegalStateException();
			if(!iter.hasNext()) throw new NoSuchElementException();
			return iter.next();
		}


		public void rewind() throws DbException, TransactionAbortedException
		{
			iter=a.iterator();
		}


		public TupleDesc getTupleDesc()
		{
			return desc;
		}


		public void close()
		{
			iter=null;
		}
	}
	public DbIterator iterator()
	{
		// some code goes here
		ArrayList<Tuple> result=new ArrayList<>();
		if(gbfield==NO_GROUPING)
		{
				Tuple temp=new Tuple(td);
				temp.setField(0,new IntField(SingleVal.get(what)));
				result.add(temp);
		}
		else if(gbfieldtype==Type.INT_TYPE)
		{
			Iterator<Integer> iter = IValMap.keySet().iterator();
			while (iter.hasNext())
			{
				Integer key=iter.next();
				Tuple temp=new Tuple(td);
				temp.setField(0,new IntField(key.intValue()));
				temp.setField(1,new IntField(IValMap.get(key).get(what)));
				result.add(temp);
			}
		}
		else
		{
			Iterator<String> iter = SValMap.keySet().iterator();
			while (iter.hasNext())
			{
				String key=iter.next();
				Tuple temp=new Tuple(td);
				temp.setField(0,new StringField(key,key.length()));
				temp.setField(1,new IntField(SValMap.get(key).get(what)));
				result.add(temp);
			}
		}
		return new IntAggIter(result,td);
	}

}
