package simpledb;

import java.io.IOException;

/**
 * The delete operator. Delete reads tuples from its child operator and removes
 * them from the table they belong to.
 */
public class Delete extends Operator
{

	private static final long serialVersionUID = 1L;
	TransactionId t;
	DbIterator child;
	boolean deleted;

	/**
	 * Constructor specifying the transaction that this delete belongs to as
	 * well as the child to read from.
	 *
	 * @param pt     The transaction this delete runs in
	 * @param pchild The child operator from which to read tuples for deletion
	 */
	public Delete(TransactionId pt, DbIterator pchild)
	{
		// some code goes here
		t=pt;
		child=pchild;
		deleted=false;
	}

	public TupleDesc getTupleDesc()
	{
		// some code goes here
		return new TupleDesc(new Type[]{Type.INT_TYPE});
	}

	public void open() throws DbException, TransactionAbortedException
	{
		// some code goes
		super.open();
		child.open();
		deleted=false;
	}

	public void close()
	{
		// some code goes here
		child.close();
		super.close();
		deleted=false;

	}

	public void rewind() throws DbException, TransactionAbortedException
	{
		// some code goes here
		child.rewind();
		deleted=false;
	}

	/**
	 * Deletes tuples as they are read from the child operator. Deletes are
	 * processed via the buffer pool (which can be accessed via the
	 * Database.getBufferPool() method.
	 *
	 * @return A 1-field tuple containing the number of deleted records.
	 * @see Database#getBufferPool
	 * @see BufferPool#deleteTuple
	 */
	protected Tuple fetchNext() throws TransactionAbortedException, DbException
	{
		// some code goes here
		if(deleted) return null;
		BufferPool pool=Database.getBufferPool();
		int count=0;
		while(child.hasNext())
		{
			try
			{
				pool.deleteTuple(t,child.next());
				count++;
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}

		}
		deleted=true;
		Tuple ret=new Tuple(new TupleDesc(new Type[]{Type.INT_TYPE}));
		ret.setField(0,new IntField(count));
		return ret;
	}

	@Override
	public DbIterator[] getChildren()
	{
		// some code goes here
		return new DbIterator[]{child};
	}

	@Override
	public void setChildren(DbIterator[] children)
	{
		// some code goes here
		assert children.length==1;
		child=children[0];
	}

}
