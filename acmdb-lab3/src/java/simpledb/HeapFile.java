package simpledb;

import java.io.*;
import java.util.*;

/**
 * HeapFile is an implementation of a DbFile that stores a collection of tuples
 * in no particular order. Tuples are stored on pages, each of which is a fixed
 * size, and the file is simply a collection of those pages. HeapFile works
 * closely with HeapPage. The format of HeapPages is described in the HeapPage
 * constructor.
 *
 * @author Sam Madden
 * @see simpledb.HeapPage#HeapPage
 */
public class HeapFile implements DbFile
{
	File f;
	TupleDesc td;

	/**
	 * Constructs a heap file backed by the specified file.
	 *
	 * @param pf the file that stores the on-disk backing store for this heap
	 *           file.
	 */
	public HeapFile(File pf, TupleDesc ptd)
	{
		// some code goes here
		f = pf;
		td = ptd;
		System.out.println(f.length());
	}

	/**
	 * Returns the File backing this HeapFile on disk.
	 *
	 * @return the File backing this HeapFile on disk.
	 */
	public File getFile()
	{
		// some code goes here
		return f;
	}

	/**
	 * Returns an ID uniquely identifying this HeapFile. Implementation note:
	 * you will need to generate this tableid somewhere ensure that each
	 * HeapFile has a "unique id," and that you always return the same value for
	 * a particular HeapFile. We suggest hashing the absolute file name of the
	 * file underlying the heapfile, i.e. f.getAbsoluteFile().hashCode().
	 *
	 * @return an ID uniquely identifying this HeapFile.
	 */
	public int getId()
	{
		// some code goes here
		return f.getAbsoluteFile().hashCode();

	}

	/**
	 * Returns the TupleDesc of the table stored in this DbFile.
	 *
	 * @return TupleDesc of this DbFile.
	 */
	public TupleDesc getTupleDesc()
	{
		// some code goes here
		return td;
	}

	// see DbFile.java for javadocs
	public Page readPage(PageId pid)
	{
		// some code goes here

		if (pid.getTableId() != this.getId())
			throw new IllegalArgumentException();
		int offset = pid.pageNumber() * BufferPool.getPageSize();
		if(offset>=f.length())
		{
			System.out.println("offset:"+offset);
			throw new IllegalArgumentException();
		}
		try
		{
			RandomAccessFile temp = new RandomAccessFile(f, "r");
			temp.seek(offset);
			byte[] data = new byte[BufferPool.getPageSize()];
			temp.read(data);
			return new HeapPage((HeapPageId) pid, data);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	// see DbFile.java for javadocs
	public void writePage(Page page) throws IOException
	{
		// some code goes here
		// not necessary for lab1
		PageId pid=page.getId();
		if(pid.getTableId()!=this.getId())
			throw new IllegalArgumentException();
		int offset = pid.pageNumber() * BufferPool.getPageSize();
		if(offset>=f.length())
		{
			System.out.println("offset:"+offset);
			//throw new IllegalArgumentException();
		}
		try
		{
			RandomAccessFile temp = new RandomAccessFile(f, "rw");
			temp.seek(offset);
			byte[] data = page.getPageData();
			temp.write(data);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Returns the number of pages in this HeapFile.
	 */
	public int numPages()
	{
		// some code goes here
		return (int) (f.length() / BufferPool.getPageSize());
	}

	// see DbFile.java for javadocs
	public ArrayList<Page> insertTuple(TransactionId tid, Tuple t) throws DbException, IOException, TransactionAbortedException
	{
		// some code goes here
		ArrayList<Page> ret=new ArrayList<>();
		HeapPage ip=null;
		for(int i=0;i<numPages();i++)
		{
			Page pp = Database.getBufferPool().getPage(tid, new HeapPageId(this.getId(),i), Permissions.READ_ONLY);
			assert pp instanceof HeapPage;
			HeapPage hp=(HeapPage) pp;
			if(hp.getNumEmptySlots()>0)
			{
				Page tp=Database.getBufferPool().getPage(tid, new HeapPageId(this.getId(),i), Permissions.READ_WRITE);
				ip=(HeapPage) tp;
				ret.add(tp);
				break;
			}
		}
		if(ip==null)
		{
			int num=numPages();
			this.writePage(new HeapPage(new HeapPageId(this.getId(),num),HeapPage.createEmptyPageData()));
			ip=(HeapPage) Database.getBufferPool().getPage(tid, new HeapPageId(this.getId(),num), Permissions.READ_WRITE);
			assert ip.getNumEmptySlots()>0;

		}
		ip.insertTuple(t);
		return ret;
		// not necessary for lab1
	}

	// see DbFile.java for javadocs
	public ArrayList<Page> deleteTuple(TransactionId tid, Tuple t) throws DbException, TransactionAbortedException
	{
		// some code goes here
		ArrayList<Page> ret=new ArrayList<>();
		Page pp=Database.getBufferPool().getPage(tid,t.getRecordId().getPageId(),Permissions.READ_WRITE);
		ret.add(pp);
		assert pp instanceof HeapPage;
		HeapPage p=(HeapPage) pp;
		p.deleteTuple(t);
		return ret;
		// not necessary for lab1
	}

	private class iter implements DbFileIterator
	{
		int pgNo;
		Iterator<Tuple> pi;
		TransactionId tid;
		HeapFile hf;
		boolean is_open;

		public iter(TransactionId ptid, HeapFile phf)
		{
			tid = ptid;
			hf = phf;
			pgNo = -1;
			pi = null;
			is_open = false;
		}

		public void open() throws DbException, TransactionAbortedException
		{
			is_open = true;
		}

		/**
		 * @return true if there are more tuples available, false if no more tuples or iterator isn't open.
		 */
		public boolean hasNext() throws DbException, TransactionAbortedException
		{
			if (!is_open)
				return false;
			if (pgNo != -1 && pi == null)
				return false;
			if (pgNo == -1 || pi.hasNext() == false)
			{
				int t = pgNo + 1;
				while (true)
				{
					if(t>=numPages()) break;
					HeapPage npage = (HeapPage) Database.getBufferPool().getPage(tid, new HeapPageId(hf.getId(), t), null);
					if (npage.iterator().hasNext())
						return true;
					t++;
				}
				return false;
			}
			return true;

		}

		/**
		 * Gets the next tuple from the operator (typically implementing by reading
		 * from a child operator or an access method).
		 *
		 * @return The next tuple in the iterator.
		 * @throws NoSuchElementException if there are no more tuples
		 */
		public Tuple next() throws DbException, TransactionAbortedException, NoSuchElementException
		{
			if (!is_open)
				throw new NoSuchElementException();
			if (pgNo != -1 && pi == null)
				return null;
			if (pgNo == -1 || pi.hasNext() == false)
			{
				pgNo++;
				while (true)
				{
					if(pgNo>=numPages()) break;
					HeapPage npage = (HeapPage) Database.getBufferPool().getPage(tid, new HeapPageId(hf.getId(), pgNo), null);
					pi = npage.iterator();
					if (pi.hasNext())
						return pi.next();
					pgNo++;
				}
				pi = null;
				return null;
			}
			return pi.next();
		}

		/**
		 * Resets the iterator to the start.
		 *
		 * @throws DbException When rewind is unsupported.
		 */
		public void rewind() throws DbException, TransactionAbortedException
		{
			pgNo = -1;
			pi = null;
		}

		/**
		 * Closes the iterator.
		 */
		public void close()
		{
			is_open = false;
		}


	}

	// see DbFile.java for javadocs
	public DbFileIterator iterator(TransactionId tid)
	{
		// some code goes here
		return new iter(tid, this);

	}

}

