package simpledb;

import java.io.Serializable;

/**
 * A RecordId is a reference to a specific tuple on a specific page of a
 * specific table.
 */
public class RecordId implements Serializable {

    private static final long serialVersionUID = 1L;
    private PageId id;
    private int tupleNo;
    /**
     * Creates a new RecordId referring to the specified PageId and tuple
     * number.
     * 
     * @param pid
     *            the pageid of the page on which the tuple resides
     * @param ptupleno
     *            the tuple number within the page.
     */
    public RecordId(PageId pid, int ptupleno) {
        // some code goes here
        id=pid;
        tupleNo=ptupleno;
    }

    /**
     * @return the tuple number this RecordId references.
     */
    public int tupleno() {
        // some code goes here
        return tupleNo;
    }

    /**
     * @return the page id this RecordId references.
     */
    public PageId getPageId() {
        // some code goes here
        return id;
    }

    /**
     * Two RecordId objects are considered equal if they represent the same
     * tuple.
     * 
     * @return True if this and o represent the same tuple
     */
    @Override
    public boolean equals(Object o) {
        // some code goes here
        if(!(o instanceof  RecordId)) return false;
        RecordId temp=(RecordId)o;
        return id.equals(temp.id) && tupleNo==temp.tupleNo;

    }

    /**
     * You should implement the hashCode() so that two equal RecordId instances
     * (with respect to equals()) have the same hashCode().
     * 
     * @return An int that is the same for equal RecordId objects.
     */
    @Override
    public int hashCode() {
        // some code goes here
        StringBuffer buff=new StringBuffer();
        buff.append(id.hashCode());
        buff.append(tupleNo);
        return Integer.parseInt(buff.toString());

    }

}
